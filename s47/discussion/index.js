// console.log("hello world ")

// const txtFirstName = document.querySelector("#txt-first-name")
// const spanFullName = document.querySelector("#span-full-name")

// txtFirstName.addEventListener("keyup", (event)=>{
// 	spanFullName.innerHTML = txtFirstName.value;
// })

// console.log(event.target)
// console.log(event.target.value)
console.log("hello world");
// "documents" refers to the whole page
// query seletor is used to select specific object from the document
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
// Alternatively, we can use the getElement functions to retreive elements
// const txtFirstName = document.getElementById("txt-first-name")
// const spanFullName = document.getElementById("span-full-name")

txtFirstName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = txtFirstName.value;
  console.log(event.target);
  console.log(event.target.value);
});

